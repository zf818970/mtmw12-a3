#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" author:  James Fallon
    contact: j.fallon@pgr.reading.ac.uk
    date:    2019-10-15
    latest:  2019-10-16

Natural Parameters & Boundary Conditions for calculating geostrophic wind.
"""
import numpy as np

# Header File #
pa = 1e5    # mean pressure (Pa)
pb = 200.   # pressure variation magnitude (Pa)
f = 1e-4    # Coriolis parameter (s^-1)
rho = 1.    # Air density (kg m^-3)
L = 2.4e6   # pressure variation length scale (m)
ymin = 0.   # minimum space dimension (m)
ymax = 1e6  # maximum space dimension (m)

def pressure(y):
    """ Calculate the pressure at given y locations 
    :param numpy.ndarray y: Spatial coordinates
    :return: pressure
    :rtype: numpy.ndarray
    """
    return pa + pb*np.cos(y*np.pi/L)

def uExact(y):
    """ Calculate the analytic geostrophic wind at given locations y
    :param numpy.ndarray y: Spatial coordinates
    :return: windspeed
    :rtype: numpy.ndarray
    """
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy):
    """ Calculate the geostrophic wind as a function of pressure gradient
    :param numpy.ndarray dpdy: Pressure gradient
    :return: windspeed
    :rtype: numpy.ndarray
    """
    return -dpdy/(rho*f)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" author:  James Fallon
    contact: j.fallon@pgr.reading.ac.uk
    date:    2019-10-15
    latest:  2019-10-16

Functions for calculating gradients
"""

def gradient_2point(f, dx):
    """ The gradient of array f assuming points are a distance dx apart. The
    function is differentiated using 2-point differences (accurate up to second
    order; ie. dx^2 error). The start and end points are calculated to first 
    order accuracy using an off-centre calculation.

    :param list f: Evaluated function to differentiate
    :param float dx: Infinitesimal (differentiation resolution), of value
                     (xmax-xmin)/len(f)
    :return: gradient of f
    :rtype: float
    """
    # Validate input f,dx
    assert len(f) > 3, "f must contain at least 3 entries"
    assert dx > 0, "dx must be greater than 0"

    # Calculate dfdx endpoints to first order accuracy, store in single a entry
    # list for future list concatenation.
    dfdx_start = [(f[1]-f[0])/dx]
    dfdx_end = [(f[-1]-f[-2])/dx]

    # Calculate remaining dfdx entries to second order accuracy
    dfdx_mid = [(f[j+1] - f[j-1])/(2*dx) for j in range(1, len(f)-1)]

    # Combine lists dfdx start+mid+end
    return dfdx_start + dfdx_mid + dfdx_end

def gradient_4point(f, dx):
    """ The gradient of array f assuming points are a distance dx apart. The
    function is differentiated using 4-point differences to fourth order
    accuracy.  The start and end points are calculated to second order accuracy
    using an off-centre 3-point calculation.

    :param list f: Evaluated function to differentiate
    :param float dx: Infinitesimal (differentiation resolution), of value
                     (xmax-xmin)/len(f)
    :return: gradient of f
    :rtype: float
    """
    # Validate input f,dx
    assert len(f) > 5, "f must contain at least 5 entries"
    assert dx > 0, "dx must be greater than 0"

    # Calculate dfdx endpoints to second order accuracy
    dfdx_start = [(4*f[1] - 3*f[0] - f[2])/(2*dx),\
                  (4*f[2] - 3*f[1] - f[3])/(2*dx)]
    dfdx_end = [(-4*f[-3] + 3*f[-2] + f[-4])/(2*dx),\
                (-4*f[-2] + 3*f[-1] + f[-3])/(2*dx)]

    # Calculate remaining dfdx entries to fourth order accuracy
    dfdx_mid = [(f[j-2] - 8*f[j-1] + 8*f[j+1] - f[j+2])/(12*dx)\
                for j in range(2, len(f)-2)]

    # Combine lists dfdx start+mid+end
    return dfdx_start + dfdx_mid + dfdx_end

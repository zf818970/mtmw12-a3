\documentclass[a4paper, 12pt]{article}
    % General document formatting
    \usepackage[margin=25mm]{geometry}
    \usepackage[utf8]{inputenc}
    \usepackage{graphicx}
    % Related to math
    \usepackage{amsmath,amssymb,amsfonts,amsthm}
    \usepackage{physics,siunitx}
    % Hyperlinks
    \usepackage{hyperref}
    
\begin{document}

\title{MTMW12 Assignment 3: Numerical Differentiation}
\author{27818970}
\maketitle

The python code accompanying this report may be located at
\url{https://gitlab.act.reading.ac.uk/zf818970/mtmw12-a3}.

\section{Introduction}

Consider a constant--density momentum equation to describe the fluid flow on a
rotating planet. Under hydrostatic balance, and neglecting real world
complications such as salinity or atmospheric boundary conditions, the momentum
equations are governed by:

\begin{align}
\pdv{\vb{u}}{t} + (\vb{v}\vdot\grad)\vb{u} +\vb{f}\cp\vb{u} = -\frac{1}{\rho}\grad_zp
\end{align}

Here $\vb{v}=\begin{pmatrix}u,v,w\end{pmatrix}$ and
$\vb{u}=\begin{pmatrix}u,v,0\end{pmatrix}$ are the complete fluid flow and
horizontal fluid flow vectors respectively. $\vb{f}$, $\rho$, and $p$ denote
the Coriolis parameter, density, and pressure respectively.

The Rossby number $\mathrm{Ro}$ is defined to interpret the magnitude of the
relative acceleration to the Coriolis acceleration. If the Rossby number is
sufficiently small, then the rotation inertial term dominates. The only term
that can balance the rotation term is the pressure term. Hence the
\emph{geostrophic balance} equations:

\begin{subequations}
\begin{align}
\label{eqn:geo}
fu = -\frac{1}{\rho}\pdv{p}{y}\\
fv = +\frac{1}{\rho}\pdv{p}{x}
\end{align}
\end{subequations}

In this report, we compare an analytical solution of this equation (the sine
curve) to a numerically differentiated solution, to scope $2$--point
differentiation.

\section{Analytical Solution}

We begin with a pressure field $p$:

\begin{align}
p = p_a + p_b \cos(\frac{y\pi}{L})\\
\end{align}

The wind $u$ is evaluated by differentiating $p$ (as in
equation~\ref{eqn:geo}):

\begin{align}
u_e = \frac{p'\pi}{\rho fL} \sin(\frac{y\pi}{L})
\end{align}

\section{Numerical Solution}

Numerical differentiation formulae can be derived by evaluating $n$ Taylor
expansions of a function $f$ about each grid point $x_0+j\Delta x$ on the
curve. Each evaluation about $f(x_0+j\Delta x)$ is denoted $f_{j+i}$ for
locations $i\Delta x$ away from $j$.

\subsection{2--Point Differentiation}

A reasonably accurate starting point is $2$--point differentiation, a method of
second order accuracy. For a function $f$ of gradient $f'$:

\begin{align}
f'_j = \frac{f_{j+1}-f_{j-1}}{2\Delta x} + \mathcal{O}(\Delta x^2)
\end{align}


\subsection{4--Point Differentiation}

It is possible to obtain more accurate estimates of the gradient by increasing
the number of surrounding points to Taylor expand. We chose to implement the
$4$--point formula as a sufficient improvement over the 2--point formula:

\begin{align}
f'_j = \frac{f_{j-2}-8f_{j-1}+8f_{j+1}-f_{j+2}}{12\Delta x} + \mathcal{O}(\Delta x^4)
\end{align}

\subsection{End Point Differentiation}

At the edges of the function $f$, it isn't possible to implement either $2$ or
$4$--point differentiation, as these formulae are centered about the point of
evaluation. Instead, using a weighted formula we can still obtain an accurate
estimate of the gradient at either the start or the end of the curve. Initially
we chose a $1$--point formula of first order accuracy:

\begin{subequations}
\begin{align}
f'_j = \frac{f_{j+1}-f_j}{\Delta x} + \mathcal{O}(\Delta x)\\
f'_j = \frac{f_j-f_{j-1}}{\Delta x} + \mathcal{O}(\Delta x)
\end{align}
\end{subequations}

Improving on this, we derive and implement the second order accurate
$3$--point formula:

\begin{subequations}
\begin{align}
f'_j = \frac{4f_{j+1}-3f_{j}-f_{j+2}}{2\Delta x} + \mathcal{O}(\Delta x^2)\\
f'_j = - \frac{4f_{j-1}-3f_{j}-f_{j-2}}{2\Delta x} + \mathcal{O}(\Delta x^2)
\end{align}
\end{subequations}

\section{Results}
\label{sec:results}
We use the parameters: $p_a=$\SI{1e5}{\pascal}, $p_a=$\SI{200}{\pascal},
$f=$\SI{1e-4}{\per\second}, $\rho=$\SI{1.2}{\kilo\gram\per\meter\cubed},
$L=$\SI{2.4e6}{\meter}, and $y:0\to$\SI{1e6}{\meter}. We divide the
domain into $N=10$ equally spaced intervals.

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{figures/both1.png}
\caption{The analytic windspeed from geostrophic balance, and the numerical
	approximation calculated using n-point difference methods.}
\label{fig:u2pt}
\end{figure}

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{figures/both2.png}
\caption{Residual errors for figure~\ref{fig:u2pt}}
\label{fig:res}
\end{figure}

\begin{figure}
\center
\includegraphics[width=0.8\textwidth]{figures/both3.png}
\caption{Fractional errors for figure~\ref{fig:u2pt}}
\label{fig:frac}
\end{figure}

In figure~\ref{fig:u2pt} both methods of numerical approximation are observed
to match the analytic equation well in the middle section, but fail at the
tails where the less accurate one--point (three--point) formulae are employed.

The accuracy of numerical approximations is best understood by calculating
residual errors. Shown in figure~\ref{fig:res}, the errors in the $2$--point
formula are most significant at $y=0$ and $y=$\SI{1000}{\kilo\meter} where the
$1$--point formulae is substituted. Scaling up the residuals in the middle
section where the $2$--point formula is actually used would reveal the error
increasing with sinusoidal shape. This sinusoidal like increase exactly follows
the magnitude of $u$, as demonstrated by the plot of fractional error in
figure~\ref{fig:frac}.

The residuals also reveal the improved performance of the higher $n$--point
formulae $n=4,3$ over $n=2,1$.

\section{Conclusions}

The numerical differentiation methods work well, producing results with errors
below $2\%$ (worst case) and below $0.1\%$ in the $4$--point mid--section (of
$\mathcal{O}(\Delta x^4)$ accuracy).

A future version of this code may improve by using a higher order end--point
differentiation method.

The $3$--point formula is much more successful in estimating the $y=0$ and
$y=$\SI{1000}{\kilo\meter} tails (although it does still invoke some error).
Because the $4$--point formula is used elsewhere, and the $3$--point is now
used for two tail points not one, the error at $100$ and \SI{900}{\kilo\meter}
has increased. It would be better to instead use the $2i$--point centered
formula for evaluating $f_i$ or $f_{N-i}$ points until $i=n$, as this would
mean using the most accurate (highest order approximation) possible for each
grid point.  Another alternative improvement would be to employ a higher order
\emph{uncentred} formula at the tails.

From Euler's formula for expanding sine as a polynomial, we should expect that
as the number of points used approaches $\infty$, the error in estimating
sinusoidal gradients will approach but never reach zero. This is not however
generally true for any function, and for any analytical equation that may be
written as a finite number of polynomial terms, the $n$--point centred
(unweighted) formula of $n^\mathrm{th}$ order accuracy should achieve perfect
estimation for a polynomial of greatest power $n$.


\appendix
\section{\texttt{differentiate.py}}
This file contains code for $2$--point and $4$--point differentiation and is
designed to be used more generally outside of the geostrophic wind
applications.

\section{\texttt{geoParameters.py}}
The parameters initroduced in section~\ref{sec:results} are included, alongside
the analytical solutions for pressure, geostrophic windspeed, and a conversion
for (numerically calculated) pressure gradient to geostrophic windspeed.

\section{\texttt{geostrophicWind.py}}
The program \texttt{geostrophicWind.py} calls \texttt{differentiate.py} and
\texttt{geoParameters.py}, in order to create the plots ~\ref{fig:u2pt},
\ref{fig:res}, and \ref{fig:frac}. Required python libraries are \texttt{numpy}
and
\texttt{matplotlib}.

\end{document}

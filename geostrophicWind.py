#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" author:  James Fallon
    contact: j.fallon@pgr.reading.ac.uk
    date:    2019-10-15

MTMW12 Assignment 3
Numerically differentiate the pressure to obtain the geostrophic wind relation
using 2-point differentiation.
"""
import numpy as np
from matplotlib import pyplot as plt
from differentiate import gradient_2point, gradient_4point

def geostrophicWind():
    """ Call on functions exact, pressure, geoWind defined in geoParameters,
    to make separate analytical calculation and numerical estimation of the
    geostrophic wind relation. Plot the two results.
    """
    # geostrophic wind physical parameters are stored elsewhere
    import geoParameters as gp

    # grid spacing
    N = 10 # divide space into N intervals
    dy = (gp.ymax-gp.ymin)/N

    # Spatial dimension y
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    
    # evaluate geostrophic windspeeds
    uExact = gp.uExact(y)
    # pressure
    p = gp.pressure(y)
    # pressure gradient, and wind using two point differences
    dpdy2 = np.array(gradient_2point(p, dy))
    dpdy4 = np.array(gradient_4point(p, dy))
    u_2point = gp.geoWind(dpdy2)
    u_4point = gp.geoWind(dpdy4)

    # Setup Plot
    font = {'size':14}
    y_km = y/1e3 # convert y to km
    xlabel = '$y$ (km)'

    # Graph to compare analytic and numerical results
    # initialise a new figure
    plt.figure(1); plt.clf()
    plt.rc('font', **font)
    # Add both solutions to plot
    plt.plot(y_km, uExact, 'k-', label='Analytic Solution')
    plt.plot(y_km, u_2point, '+', color='red',\
             label='Two-Point difference', ms=12,\
             markeredgewidth=1.5, markerfacecolor='none')
    plt.plot(y_km, u_4point, 'x', color='green',\
             label='Four-Point difference', ms=12,\
             markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel(xlabel)
    plt.ylabel('$u$ (ms$^{-1}$)')
    title="Analytic Solution & Numerical Estimation of\n Geostrophic Wind Speed"
    plt.title(title)
    plt.tight_layout()

    # Plot Residuals
    residual2 = u_2point - uExact
    residual4 = u_4point - uExact
    plt.figure(2); plt.clf()
    plt.rc('font', **font)
    plt.plot(y_km, residual2, '+', color='red', ms=12,\
             markeredgewidth=1.5, markerfacecolor='none', label='2-point')
    plt.plot(y_km, residual4, 'x', color='green', ms=12,\
             markeredgewidth=1.5, markerfacecolor='none', label='4-point')
    plt.axhline(0, np.min(y_km), np.max(y_km), linestyle='--', color='k')
    plt.legend(loc='best')
    plt.xlabel(xlabel)
    plt.ylabel('$\Delta u$ (ms$^{-1}$)')
    plt.title("Geostrophic Wind Speed\n Numerical Estimation Residuals")
    plt.tight_layout()

    # Plot Fractional Error
    # express fractional error as a percentage
    fractional2 = abs(u_2point/uExact-1)*100
    fractional4 = abs(u_4point/uExact-1)*100
    # if numerical solution == analytic solution == 0 ensure error is
    # correctly stated
    for i in np.where(uExact==0):
        if u_2point[i] == 0:
            fractional2[i] = 0
        else:
            fractional2[i] = np.inf
        if u_4point[i] == 0:
            fractional4[i] = 0
        else:
            fractional4[i] = np.inf

    plt.figure(3); plt.clf()
    plt.rc('font', **font)
    plt.plot(y_km, fractional2, '+', color='red', ms=12,\
             markeredgewidth=1.5, markerfacecolor='none',\
             label='Fractional Error (2-point)')
    plt.plot(y_km, fractional4, 'x', color='green', ms=12,\
             markeredgewidth=1.5, markerfacecolor='none',\
             label='Fractional Error (4-point)')
    plt.xlabel(xlabel)
    plt.ylabel('Fractional Error ($\%$)')
    plt.title("Geostrophic Wind Speed\n Numerical Estimation Error")
    # Demarcate where divide by zero encountered
    for frac in (fractional2,fractional4):
        if np.inf in frac:
            divis_error = np.where(frac==np.inf)
            plt.plot(divis_error,'k*', label='Division by 0 Encountered', ms=12)
    plt.legend()
    plt.tight_layout()

    # Display Plots
    plt.show()
    return

if __name__ == "__main__":
    geostrophicWind()
